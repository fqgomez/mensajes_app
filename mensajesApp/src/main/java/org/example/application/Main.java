package org.example.application;

import org.example.application.configuration.Conexion;
import org.example.infrastructure.MensajeService;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int opcion = 0;

        do{
            System.out.println("-------------------");
            System.out.println(" Aplicaion de Mensajes ");
            System.out.println(" 1. crear mensaje");
            System.out.println(" 2. listar mensajes");
            System.out.println(" 3. editar mensaje");
            System.out.println(" 4. eliminar mensaje");
            System.out.println(" 5. salir");

            opcion = sc.nextInt();

            switch(opcion){
                case 1:
                    MensajeService.crearMensaje();
                    break;

                case 2:
                    MensajeService.listarMensajes();
                    break;

                case 3:
                    MensajeService.editarMensaje();
                    break;

                case 4:
                    MensajeService.borrarMensaje();
                    break;
            }
        }while(opcion != 5);


    }
}